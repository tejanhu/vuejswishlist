import Vue from 'vue'
import App from './App.vue'
import Navbar from './Navbar.vue'
import CreateList from './CreateList'

Vue.component('navbar', Navbar)
Vue.component('createList', CreateList)


new Vue({
  el: '#app',
  render: h => h(App)
})
